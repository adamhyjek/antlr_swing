package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.*;

public class MyTreeParser extends TreeParser {
	
	protected GlobalSymbols globalSymbols = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(Integer e1, Integer e2) {
		return e1+e2;
	}
	
	protected Integer substract(Integer e1, Integer e2) {
		return e1-e2;
	}
	
	protected Integer multiply(Integer e1, Integer e2) {
		return e1*e2;
	}
	
	protected Integer divide(Integer e1, Integer e2) {
		return e1/e2;
	}
	
	
	protected void declareVariable(String label) {
		globalSymbols.newSymbol(label);
	}
	
	protected Integer getVariable(String label) {
		return globalSymbols.getSymbol(label);
	}
	
	protected Integer setVariable(String label, Integer value) {
		globalSymbols.setSymbol(label, value);
		return value;
	}
}
